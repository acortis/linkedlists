﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedListClasses.DoublyLinkedList
{
    public class DoublyLinkedList<T>
    {
        public int Size { get; internal set; }
        public DoublyLinkedNode<T> Head { get; internal set; }

        public void InsertFirst(T elem)
        {
            // Create the newNode
            DoublyLinkedNode<T> newNode = new DoublyLinkedNode<T>();
            newNode.Element = elem;

            // Place the newNode at the start
            newNode.Next = Head;

            // We can only set Head.Prev if Head exists!
            if (Head != null)
            {
                Head.Prev = newNode; // Step 3
            }

            // Update head to point to the new node
            Head = newNode;

            Size++;
        }

        public void InsertAfter(DoublyLinkedNode<T> currentNode, T elem)
        {
            if (currentNode == null) // currentNode does not exist!
            {
                throw new InvalidOperationException("Cannot insert an element after a null!");
            }
            
            // step 1: Create the new node.
            DoublyLinkedNode<T> newNode = new DoublyLinkedNode<T>();
            newNode.Element = elem;

            // step 2:
            newNode.Prev = currentNode;

            // step 3:
            newNode.Next = currentNode.Next;

            // If currentNode.Next does not exist
            // That is, if currentNode is the last node in the list
            if (currentNode.Next != null)
            {
                // we cannot set currentNode.Next.Prev since currentNode.Next is null

                // step 4:
                currentNode.Next.Prev = newNode;
            }

            // step 5:
            currentNode.Next = newNode;
            
            // step 6:
            Size++;
        }
    }
}
