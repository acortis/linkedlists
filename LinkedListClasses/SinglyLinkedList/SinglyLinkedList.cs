﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedListClasses.SinglyLinkedList
{
    public class SinglyLinkedList<T>
    {
        public int Size { get; internal set; } = 0;

        /// <summary>
        /// Represents the first Node in the linked list
        /// </summary>
        public Node<T> Head { get; internal set; }

        public void InsertFirst(T elem) {

            // "Constructive" operations first
            Node<T> newNode = new Node<T>();
            newNode.Element = elem;
            newNode.Next = Head;
            
            Size++;

            // "Destructive" operations (which change existing data)
            Head = newNode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// About 1 operation (regardless of Size!)
        /// </remarks>
        public T RemoveFirst() {

            if (Head == null) // List is empty and there is nothing to remove!
            {
                throw new InvalidOperationException("Cannot remove element from an empty list!");
            }
            // else Head exists and is not null

            // get element to output
            T output = Head.Element;

            // skip head (i.e. remove first element)
            Head = Head.Next;

            // reduce size
            Size--;

            // finish and return output
            return output;
        }

        public void InsertLast(T elem)
        {
            throw new NotImplementedException();
        }

        public T RemoveLast(T elem)
        {
            throw new NotImplementedException();
        }

        public void InsertBefore(Node<T> currentNode, T elem)
        {
            // currentNode is the first node in the list
            if (currentNode == Head)
            {
                // inserting BEFORE the first node is equivalent to InsertFirst
                InsertFirst(elem);
                return;
            }

            // otherwise, we know how to InsertAfter AND this is NOT the firstNode in the list...
            // Careful with issues like currentNode == null AND currentNode is NOT within the list at all!
            Node<T> previousNode = PreviousNode(currentNode);

            // Now that we have the previousNode,
            // The InsertBefore is equivalent to InsertAfter the previousNode
            InsertAfter(previousNode, elem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentNode"></param>
        /// <param name="elem"></param>
        /// <remarks>
        /// This method is independent of the number of elements in the list.
        /// Therefore, regardless of the number of elements, it will always take the same time.
        /// This method will take "about" 1 operation.
        /// </remarks>
        public void InsertAfter(Node<T> currentNode, T elem)
        {
            if (currentNode == null) // currentNode does not exist!
            {
                throw new InvalidOperationException("Cannot insert an element after a null!");
            }

            // ToDo: Check that the currentNode actually belongs to the same LinkedList (efficiently! i.e. in about 1 operation)

            // Constructive operations first
            Node<T> newNode = new Node<T>()
            {
                Element = elem,
                Next = currentNode.Next
            };

            currentNode.Next = newNode;

            Size++;
        }

        public T RemoveBefore(Node<T> currentNode)
        {
            throw new NotImplementedException();
        }

        public T RemoveAfter(Node<T> currentNode)
        {
            if (currentNode == null) // currentNode does not exist!
            {
                throw new InvalidOperationException("Cannot remove an element after a null!");
            }

            if (currentNode.Next == null) // currentNode is the last node!
            {
                throw new InvalidOperationException("The is no node to remove after the current node!");
            }

            T output = currentNode.Next.Element;

            currentNode.Next = currentNode.Next.Next;

            Size--;
            
            return output;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentNode"></param>
        /// <returns></returns>
        /// <remarks>
        /// There are no loops. There is only "about" 1 operation required.
        /// </remarks>
        private Node<T> NextNode(Node<T> currentNode)
        {
            if (currentNode == null)
            {
                // calling currentNode.Next would cause a NullReferenceException to be thrown
                throw new ArgumentNullException("currentNode");
            }

            return
                currentNode.Next;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentNode"></param>
        /// <returns></returns>
        /// <remarks>
        /// Loop goes through the nodes, one at a time.
        /// Worst case, it may have to go through all of the n nodes.
        /// Worst case, this takes about n operations.
        /// </remarks>
        private Node<T> PreviousNode(Node<T> currentNode)
        {
            if (currentNode == null)
            {
                // calling currentNode.Next would cause a NullReferenceException to be thrown
                throw new ArgumentNullException("currentNode");
            }

            // The following link does not exist!
            // return currentNode.Prev;

            // We have to start from Head of the list,
            // and look forward from there...

            if (Head == currentNode) // the currentNode is the first node of the list!
            {
                return null; // No nodes before the currentNode!
            }

            Node<T> cursor = Head; // start by looking at the first node

            while (cursor != null) // while we are still within the list...
            {
                // If the next node is the node that we are asking for it's "previous"
                if (cursor.Next == currentNode)
                {
                    return cursor; // this is the node that we are looking for!
                }

                // Move to the next node...
                cursor = cursor.Next;
            }
            
            // currentNode not found!
            throw new InvalidOperationException(
                "currentNode is not in the linked list! Cannot get previous!");
            
        }

        public override string ToString()
        {
            return String.Format("[ SinlyLinkedList with {0} elements ]", Size);
        }
    }
}
