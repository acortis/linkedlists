﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedListClasses.SinglyLinkedList
{
    /// <summary>
    /// ToDo: Refactor this class and move to it's own namespace!
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Queue<T>
    {
        private SinglyLinkedList<T> linkedList = new SinglyLinkedList<T>();
        private Node<T> tail = null;

        public void Enqueue(T elem)
        {
            // What cases are there when the tail is NOT in the linked list?

            // case 1: tail == null (and therefore the linkedList is empty!)
            // case 2: linkedList is empty (and therefore no element is within the list!)
            if (tail == null || linkedList.Size == 0)
            {
                // this is an empty queue.. linkedList is empty!
                linkedList.InsertFirst(elem); // list is empty, so just add the element
                tail = linkedList.Head;       // this is the only element in the list!
                return;
            }
            
            // *assuming* that the tail is already within the list,
            // (this assumption is true, since we have already confirmed it!)

            // insert after the tail
            linkedList.InsertAfter(tail, elem);

            // a new node has been added, move the tail 1 step forward
            tail = tail.Next;
        }

        public T Dequeue()
        {
            T output = linkedList.RemoveFirst();


            if (linkedList.Size == 0)
            {
                tail = null;
            }

            return output;
        }
    }
}
