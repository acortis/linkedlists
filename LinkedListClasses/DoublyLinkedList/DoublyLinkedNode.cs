﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedListClasses.SinglyLinkedList
{
    public class DoublyLinkedNode<T>
    {
        public T Element { get; set; }

        public DoublyLinkedNode<T> Prev { get; internal set; }
        public DoublyLinkedNode<T> Next { get; internal set; }

        public override string ToString()
        {
            return
                String.Format(
                    "[ Node with element: {0} ]", Element);
        }
    }
}
