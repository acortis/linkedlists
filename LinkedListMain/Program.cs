﻿using LinkedListClasses.SinglyLinkedList;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedListMain
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();

            LinkedListClasses.SinglyLinkedList.Queue<int> queue =
                new LinkedListClasses.SinglyLinkedList.Queue<int>();

            // Execute the method once before timing...
            // If there is anything left to compile, or any other additional work related to starting up the method,
            // it will be carried out here, before the actual timed method call.
            queue.Enqueue(0);

            int repetitions = 10000;
            
            for (int i = 0; i < repetitions; i++)
            {
                // Repeat the method call a number of times, so that the average delay from background noise and context switching is taken
                sw.Start();

                queue.Enqueue(0);

                sw.Stop();
            }
            
            Console.WriteLine(
                String.Format("Total time for performing the above operation {1} times, took about {0} ticks", sw.ElapsedTicks, repetitions));

            Console.WriteLine(
                String.Format("Mean (average) time for performing the above operation is about {0} ticks", (double)sw.ElapsedTicks/repetitions));

            sw.Reset(); // set the stopwatch back to 0!

            Console.ReadKey();

            /*
            SinglyLinkedList<String> myList = new SinglyLinkedList<string>();

            myList.InsertFirst("E");
            myList.InsertFirst("D");
            myList.InsertFirst("B");
            myList.InsertFirst("A");


            // LinkedList was A, B, D and E

            // CurrentNode is the node with element B
            Node<String> currentNode = myList.Head.Next;

            myList.InsertAfter(currentNode, "C");

            // LinkedList is now A, B, *C*, D and E
            
            Console.WriteLine(
                    myList.RemoveFirst());

            Console.WriteLine(
                    myList.RemoveFirst());

            Console.WriteLine(
                    myList.RemoveFirst());

            // List is empty, calling RemoveFirst
            Console.WriteLine(
                    myList.RemoveFirst());
                    */

        }
    }
}
